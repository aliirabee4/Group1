import 'package:flutter/material.dart';
import 'package:group1/home_appbar.dart';
import 'package:group1/offer_card.dart';
import 'package:group1/places_list.dart';
import 'package:group1/text_field.dart';

class MsgScreen extends StatefulWidget {
  @override
  _MsgScreenState createState() => _MsgScreenState();
}

class _MsgScreenState extends State<MsgScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color.fromRGBO(247, 247, 247, 1),
      appBar: homeAppabr(),
      body: Column(
        children: [HomeTextField(), OfferCard(), PlacesList()],
      ),
    );
  }
}
