import 'package:flutter/material.dart';
import 'package:group1/home.dart';
import 'package:group1/msg.dart';
import 'package:group1/notification.dart';
import 'package:group1/search.dart';

class BottomNavigayionScreen extends StatefulWidget {
  @override
  _BottomNavigayionScreenState createState() => _BottomNavigayionScreenState();
}

class _BottomNavigayionScreenState extends State<BottomNavigayionScreen> {
  int _currentIndex = 0;
  List _screens = [
    HomeScreen(),
    SearchScreen(),
    NotificationScreen(),
    MsgScreen()
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   centerTitle: true,
      //   title: Text((() {
      //     if (_currentIndex == 0) {
      //       return 'Home';
      //     } else if (_currentIndex == 1) {
      //       return 'Search';
      //     } else if (_currentIndex == 2) {
      //       return 'Notifications';
      //     } else {
      //       return 'Msg';
      //     }
      //   }())),
      // ),
      body: _screens[_currentIndex],
      bottomNavigationBar: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          currentIndex: _currentIndex,
          onTap: (index) {
            setState(() {
              _currentIndex = index;
            });
          },
          items: [
            BottomNavigationBarItem(
                icon: Icon(
                  Icons.home,
                  size: 25,
                ),
                title: Text(
                  '',
                  style: TextStyle(fontSize: 0),
                )),
            BottomNavigationBarItem(
                icon: Icon(
                  Icons.search,
                  size: 25,
                ),
                title: Text(
                  '',
                  style: TextStyle(fontSize: 0),
                )),
            BottomNavigationBarItem(
                icon: Icon(
                  Icons.notifications,
                  size: 25,
                ),
                title: Text(
                  '',
                  style: TextStyle(fontSize: 0),
                )),
            BottomNavigationBarItem(
                icon: Icon(
                  Icons.mail_outline,
                  size: 25,
                ),
                title: Text(
                  '',
                  style: TextStyle(fontSize: 0),
                )),
          ]),
    );
  }
}
