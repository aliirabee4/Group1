import 'package:flutter/material.dart';
import 'package:group1/place_card.dart';

class PlacesList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: ListView.builder(
            itemCount: 20,
            itemBuilder: (ctx, index) {
              return PlaceCard();
            }));
  }
}
