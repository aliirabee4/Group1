import 'package:flutter/material.dart';

import 'bottom_navigation.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: BottomNavigayionScreen(),
    );
  }
}

///////////////////////////////////////////////////

// class HomeScreen extends StatefulWidget {
//   @override
//   _HomeScreenState createState() => _HomeScreenState();
// }

// class _HomeScreenState extends State<HomeScreen> {
//   Widget _buildContainer(int index) {
//     return Container(
//       width: 100,
//       height: 100,
//       margin: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
//       decoration: BoxDecoration(
//           image: DecorationImage(
//               image: NetworkImage(
//                   'https://img.freepik.com/free-vector/people-eating-food-court-cafeterias_74855-5284.jpg?size=626&ext=jpg&ga=GA1.2.205326237.1597520898'),
//               fit: BoxFit.cover),
//           color: Colors.red,
//           borderRadius: BorderRadius.circular(15)),
//       child: Center(
//         child: Text(index.toString()),
//       ),
//     );
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         backgroundColor: Colors.green,
//         title: Text('First App'),
//         centerTitle: true,
//         actions: [
//           IconButton(
//               icon: Icon(
//                 Icons.search,
//                 color: Colors.white,
//               ),
//               onPressed: null),
//           IconButton(
//               icon: Icon(
//                 Icons.shopping_basket,
//                 color: Colors.white,
//               ),
//               onPressed: null)
//         ],
//         leading: IconButton(
//             icon: Icon(
//               Icons.arrow_back_ios,
//               color: Colors.white,
//             ),
//             onPressed: () {}),
//       ),

//       body: Column(
//         children: [
//           Stack(
//             children: [
//               Container(
//                 width: MediaQuery.of(context).size.width,
//                 height: MediaQuery.of(context).size.height / 2,
//                 color: Colors.red,
//               ),
//               Positioned(
//                   top: 100,
//                   left: 120,
//                   child: Container(
//                     width: 100,
//                     height: 100,
//                     color: Colors.green,
//                   )),
//               Positioned(
//                 top: 220,
//                 left: 100,
//                 child: Container(
//                   width: 100,
//                   height: 100,
//                   color: Colors.yellow,
//                 ),
//               )
//             ],
//           )
//         ],
//       ),
//       // body: ListView.builder(
//       //   itemCount: 5,
//       //   itemBuilder: (context, index) {
//       //     return _buildContainer(index);
//       //   },
//       // )
//       // body: Container(
//       //   color: Colors.white,
//       //   child: Column(
//       //     mainAxisAlignment: MainAxisAlignment.center,
//       //     children: [
//       //       Container(
//       //         width: 150,
//       //         height: 150,
//       //         decoration: BoxDecoration(
//       //             color: Colors.red, borderRadius: BorderRadius.circular(20)),
//       //         child: Center(
//       //           child: Text(
//       //             'Welcome',
//       //             style: TextStyle(fontSize: 12),
//       //           ),
//       //         ),
//       //       )
//       //     ],
//       //   ),
//       // ),
//     );
//   }
// }
