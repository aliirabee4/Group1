import 'package:flutter/material.dart';

Widget homeAppabr() {
  return AppBar(
    elevation: 0,
    backgroundColor: Color.fromRGBO(247, 247, 247, 1),
    leading: Stack(
      children: [
        IconButton(
            icon: Icon(
              Icons.notifications,
              size: 30,
              color: Color.fromRGBO(67, 84, 160, 1),
            ),
            onPressed: null),
        Positioned(
          top: 10,
          left: 25,
          child: Container(
            width: 15,
            height: 15,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
                color: Color.fromRGBO(224, 130, 180, 1)),
            child: Center(
              child: Text(
                '1',
                style: TextStyle(color: Colors.white),
              ),
            ),
          ),
        )
      ],
    ),
    actions: [
      Container(
        margin: EdgeInsets.only(right: 5, top: 4),
        width: 50,
        height: 50,
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage('assets/images/homeLogo.png'),
                fit: BoxFit.cover),
            borderRadius: BorderRadius.circular(50),
            color: Colors.white),
      )
    ],
  );
}
